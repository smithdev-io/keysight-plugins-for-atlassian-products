<h3>Introduction</h3>

<p>The AUI Pages Tabs macro constructs a tabbed graphical display of a number
of Confluence pages that have a specific label.
</p>

<h3>Parameters</h3>

<p><strong>Label</strong>: The label on pages that should be pulled into the tabbed structure.</p>
<p><strong>Limit</strong>: The number of pages to pull in. 100 is the maximum number</p>
<p><strong>Vertical</strong>: If checked, the tabs will be shown vertically rather than horizontally.</p>
<p><strong>On Creation Date</strong>: If set, the version of the included pages that existed when the containing page was created will be shown.</p>
<p><strong>On Date</strong>: If checked, the tabs will be listed vertically rather than horizontally.</p>
<p><strong>Left Truncation</strong>: As the page title is inserted into a tab label, this is the number of characters to truncate from the left.</p>
<p><strong>Right Truncation</strong>: As the page title is inserted into a tab label, this is the number of characters to truncate from the right.</p>

<p>Note, if no version of the page existed prior to the bounding date, the first version of the page will be shown.</p>
