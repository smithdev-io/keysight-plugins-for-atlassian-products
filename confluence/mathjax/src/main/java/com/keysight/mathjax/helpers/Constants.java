package com.keysight.mathjax.helpers;

// Class used to contain shared static configuration details
abstract class Constants {
    // The url to used to serve Mathjax before the user provides one
    static String DEFAULT_URL = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML";
}