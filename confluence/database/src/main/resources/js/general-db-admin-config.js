databaseConfigHelper = (function($) {

    var methods = new Object();
    var url = AJS.contextPath() + "/rest/database/1.0/admin-config/configuration";
    // This dictates whether the stored xml is still valid.
    // Follows the Semver standard (First digit indicates breaking change).
    var schemaVersionString = "1.1.0";
    var schemaVersionArr = schemaVersionString.split(".");

    methods['saveJdbcDriverDirectory'] = function() {
        saveConfig();
        location.reload();
    }

    methods['saveConfigAuth'] = function() {
        saveConfig();
    }

    methods['addGroup'] = function() {
        if (AJS.$("#group-list").length == 0) {
            AJS.$("#group-list-container").html("<ul id=\"group-list\"></ul>");
        }

        appendGroup(AJS.$("#new-group-name").val());
        AJS.$("#new-group-name").val("");

        saveConfig();
    }

    methods['removeEntry'] = function(e) {
        e.preventDefault();
        $(e.currentTarget).parent().remove();

        if (AJS.$(".saved-group").length == 0) {
            AJS.$("#group-list-container").html("<div class=\"none-saved\">No saved groups</div>");
        }

        if (AJS.$(".saved-address").length == 0) {
            AJS.$("#email-list-container").html("<div class=\"none-saved\">No saved addresses.</div>");
        }

        saveConfig();
    }

    methods['loadConfig'] = function() {
        $.ajax({
            url: url,
            dataType: "json"
        }).done(function(pluginConfiguration) {
            var defaultSchema = "<plugin-configuration>\n"+
                                "    <jdbc-driver-directory></jdbc-driver-directory>\n"+
                                "    <microsoft-driver-url></microsoft-driver-url>\n"+
                                "    <derby-driver-url></derby-driver-url>\n"+
                                "    <mongo-driver-url></mongo-driver-url>\n"+
                                "    <mysql-driver-url></mysql-driver-url>\n"+
                                "    <pgsql-driver-url></pgsql-driver-url>\n"+
                                "    <config-authorized-users></config-authorized-users>\n"+
                                "    <config-authorized-groups></config-authorized-groups>\n"+
                                "    <row-limit-type></row-limit-type>\n"+
                                "    <row-limit-value></row-limit-value>\n"+
                                "    <timeout-limit-type></timeout-limit-type>\n"+
                                "    <timeout-limit-value></timeout-limit-value>\n"+
                                "    <log-email></log-email>\n"+
                                "    <notification-email></notification-email>\n"+
                                "</plugin-configuration>";

            // If XML doesn't validate, just give an empty document
            try {
                $xml = AJS.$(AJS.$.parseXML(decodeURIComponent(pluginConfiguration.xml)));
            } catch(err) {
                if( pluginConfiguration.xml != null){
                   console.error("Failed to parse XML: ", err);
                }
                $xml = AJS.$(AJS.$.parseXML(defaultSchema));
            }

            // Check for breaking changes in the schema
            if ($xml.find("schema-version").length != 0) {
                var xmlVersion = $xml.find("schema-version").html();
                var xmlMajorVer = xmlVersion.split(".")[0];
                if (xmlMajorVer != schemaVersionArr[0]) {
                    $xml = AJS.$(AJS.$.parseXML(defaultSchema));
                }
            }

            // Get url values and default them to below constants if they aren't set
            var microsoftDriverUrl = $xml.find("microsoft-driver-url").html();
            var derbyDriverUrl = $xml.find("derby-driver-url").html();
            var mongoDriverUrl = $xml.find("mongo-driver-url").html();
            var mysqlDriverUrl = $xml.find("mysql-driver-url").html();
            var pgsqlDriverUrl = $xml.find("pgsql-driver-url").html();

            if (!(microsoftDriverUrl)) {
                microsoftDriverUrl = "http://central.maven.org/maven2/com/microsoft/sqlserver/mssql-jdbc/6.1.0.jre8/mssql-jdbc-6.1.0.jre8.jar";
            }
            if (!(derbyDriverUrl)) {
                derbyDriverUrl = "http://central.maven.org/maven2/org/apache/derby/derby/10.12.1.1/derby-10.12.1.1.jar";
            }
            if (!(mongoDriverUrl)) {
                mongoDriverUrl = "http://central.maven.org/maven2/org/mongodb/mongodb-driver/3.4.2/mongodb-driver-3.4.2.jar";
            }
            if (!(mysqlDriverUrl)) {
                mysqlDriverUrl = "http://central.maven.org/maven2/mysql/mysql-connector-java/6.0.6/mysql-connector-java-6.0.6.jar";
            }
            if (!(pgsqlDriverUrl)) {
                pgsqlDriverUrl = "https://jdbc.postgresql.org/download/postgresql-42.1.1.jar";
            }
;
            $("#jdbc-driver-directory").val(atob($xml.find("jdbc-driver-directory").html()));
            $("#microsoft-driver-url").val(microsoftDriverUrl);
            $("#derby-driver-url").val(derbyDriverUrl);
            $("#mongo-driver-url").val(mongoDriverUrl);
            $("#mysql-driver-url").val(mysqlDriverUrl);
            $("#pgsql-driver-url").val(pgsqlDriverUrl);


            // Pulling other settings from the xml
            var configAuthorizedUsers = atob($xml.find("config-authorized-users").html());
            var configAuthorizedGroups = atob($xml.find("config-authorized-groups").html());
            var rowLimitType = atob($xml.find("row-limit-type").html());
            var rowLimitValue = atob($xml.find("row-limit-value").html());
            var timeoutLimitType = atob($xml.find("timeout-limit-type").html());
            var timeoutLimitValue = atob($xml.find("timeout-limit-value").html());
            var notifyAll = $xml.find("log-email").html();
            var notificationEmail = atob($xml.find("notification-email").html());

            // If we failed to get the fields from the xml, default to empty string;
            if (configAuthorizedUsers == null) {
                configAuthorizedUsers = "";
            }
            if (configAuthorizedGroups == null) {
                configAuthorizedGroups = "";
            }
            if (rowLimitType == null) {
                rowLimitType = "Soft";
            }
            if (rowLimitValue == null) {
                rowLimitValue = "";
            }
            if (timeoutLimitType == null) {
                timeoutLimitType = "Soft";
            }
            if (timeoutLimitValue == null) {
                timeoutLimitValue = "";
            }
            if (notificationEmail == null) {
                notificationEmail = "";
            }

            // Load access profiles for this page.
            AJS.$("#configuration-permissions").append(Keysight.Database.Admin.Config.Templates.accessProfile({
                configAuthorizedUsers: configAuthorizedUsers,
                configAuthorizedGroups: configAuthorizedGroups
            }));

            AJS.$("#row-limit-type").val( rowLimitType );
            AJS.$("#row-limit-value").val( rowLimitValue );
            AJS.$("#timeout-limit-type").val( timeoutLimitType );
            AJS.$("#timeout-limit-value").val( timeoutLimitValue );
            AJS.$("#log-email").val(notifyAll);
            AJS.$("#notification-email").val( notificationEmail );

            AJS.$("#save-config-auth").click(function(e) {
                e.preventDefault();
                databaseConfigHelper.saveConfigAuth();
            });

            AJS.$("#save-performance-security-control-settings").click(function(e) {
                e.preventDefault();
                databaseConfigHelper.saveConfigAuth();
            });

            AJS.$("#save-custom-cdn").click(function(e) {
                e.preventDefault();
                databaseConfigHelper.saveConfigAuth();
            });

        }).fail(function(self, status, error) {
            var loadFlag = AJS.flag({
                type: 'error',
                title: 'Failed to fetch configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }



    function saveConfig() {
        function escapeXml(unsafe) {
            return unsafe.replace(/[<>&'"]/g, function (c) {
                switch (c) {
                    case '<': return '&lt;';
                    case '>': return '&gt;';
                    case '&': return '&amp;';
                    case '\'': return '&apos;';
                    case '"': return '&quot;';
                }
            });
        }

        var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n" +
            '<plugin-configuration>' + "\n" +
               '<schema-version>'+schemaVersionString+'</schema-version>' + "\n" +
            '   <jdbc-driver-directory>' + btoa($("#jdbc-driver-directory").val()) + "</jdbc-driver-directory>\n" +
            '    <microsoft-driver-url>'+escapeXml($("#microsoft-driver-url").val())+'</microsoft-driver-url>\n'+
            '    <derby-driver-url>'+escapeXml($("#derby-driver-url").val())+'</derby-driver-url>\n'+
            '    <mongo-driver-url>'+escapeXml($("#mongo-driver-url").val())+'</mongo-driver-url>\n'+
            '    <mysql-driver-url>'+escapeXml($("#mysql-driver-url").val())+'</mysql-driver-url>\n'+
            '    <pgsql-driver-url>'+escapeXml($("#pgsql-driver-url").val())+'</pgsql-driver-url>\n'+
            '   <config-authorized-users>' + btoa($("#config-authorized-users").val()) + "</config-authorized-users>\n" +
            '   <config-authorized-groups>' + btoa($("#config-authorized-groups").val()) + "</config-authorized-groups>\n" +
            '   <row-limit-type>' + btoa($("#row-limit-type").val()) + "</row-limit-type>\n" +
            '   <row-limit-value>' + btoa($("#row-limit-value").val()) + "</row-limit-value>\n" +
            '   <timeout-limit-type>' + btoa($("#timeout-limit-type").val()) + "</timeout-limit-type>\n" +
            '   <timeout-limit-value>' + btoa($("#timeout-limit-value").val()) + "</timeout-limit-value>\n" +
            '   <log-email>' + $("#log-email").val() + "</log-email>\n" +
            '   <notification-email>' + btoa($("#notification-email").val()) + "</notification-email>\n" +
            '</plugin-configuration>' + "\n";

        // Validate XML
        try {
            AJS.$(AJS.$.parseXML(xmlString))
        } catch(err) {
            console.error("Malformed XML!: ", err);
            return;
        }

        AJS.$.ajax({
            url: url,
            type: "PUT",
            contentType: "application/json",
            data: '{"xml":"' + encodeURIComponent(xmlString) + '"}',
            processData: false
        }).done(function() {
            var saveSuccessFlag = AJS.flag({
                type: 'success',
                title: 'Success!',
                body: 'Plugin configuration was saved successfully.',
                close: 'auto'
            });
        }).fail(function(self, status, error) {
            var saveFailFlag = AJS.flag({
                type: 'error',
                title: 'Failed to save configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    return methods;
})(AJS.$ || jQuery);

AJS.toInit(function() {

    AJS.$("#save-jdbc-driver-directory").click(function(e) {
        e.preventDefault();
        databaseConfigHelper.saveJdbcDriverDirectory();
    });

    databaseConfigHelper.loadConfig();
});