package com.keysight.database.macros;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;

public class DatabaseQueryCompact extends DatabaseQuery {

    public DatabaseQueryCompact( PageManager pageManager,
                                 PluginSettingsFactory pluginSettingsFactory,
                                 SettingsManager settingsManager,
                                 SpaceManager spaceManager,
                                 MultiQueueTaskManager taskManager,
                                 TransactionTemplate transactionTemplate,
                                 UserAccessor userAccessor,
                                 UserManager userManager,
                                 VelocityHelperService velocityHelperService
    ) {
        super( pageManager, pluginSettingsFactory, settingsManager, spaceManager,
               taskManager, transactionTemplate, userAccessor, userManager, velocityHelperService);
    }

    public BodyType getBodyType() { return BodyType.NONE; }
}
