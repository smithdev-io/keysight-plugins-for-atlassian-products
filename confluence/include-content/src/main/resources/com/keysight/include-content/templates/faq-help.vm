#set( $images = "${webResourceHelper.getStaticResourcePrefix()}/download/resources/com.keysight.include-content:include-content-resources/images" )
#set( $expandImage = "$images/expand.jpg" )
#set( $expandAllItemsImage = "$images/expandAllItems.jpg" )
#set( $linkImage = "$images/link.jpg" )
#set( $linkAllItemsImage = "$images/linkAllItems.jpg" )
#set( $popupImage = "$images/popup.jpg" )

#set( $qAsHtml = '"' )
#set( $checkAsHtml    = "<img class=${qAsHtml}emoticon emoticon-tick${qAsHtml} title=${qAsHtml}(tick)${qAsHtml} src=${qAsHtml}$images/check.png${qAsHtml} alt=${qAsHtml}(tick)${qAsHtml} border=${qAsHtml}0${qAsHtml} />" )
#set( $crossAsHtml    = "<img class=${qAsHtml}emoticon emoticon-cross${qAsHtml} title=${qAsHtml}(cross)${qAsHtml} src=${qAsHtml}$images/cross.png${qAsHtml} alt=${qAsHtml}(cross)${qAsHtml} border=${qAsHtml}0${qAsHtml} />" )
#set( $questionAsHtml = "<img class=${qAsHtml}emoticon emoticon-question${qAsHtml} title=${qAsHtml}(question)${qAsHtml} src=${qAsHtml}$images/question.png${qAsHtml} alt=${qAsHtml}(question)${qAsHtml} border=${qAsHtml}0${qAsHtml} />" )

<h3>Introduction</h3>
<p>The original idea behind the <strong>faq</strong> macro is to collect the content
from a number of pages into a list of <strong>expand</strong> macros.  The most common
use case is to build the list from the child pages.  For some, the solution may not look
as clean as a single page with a series of <strong>expand</strong> macros because the
page tree would show the questions; however the author of this plugin found it easier to
maintain as the <strong>expand</strong> macro does a poor job of showing the link text
when in the editor.</p>

<p><img src="$expandImage" width="500px" alt="View of FAQ macro with Render Style set to Expand" /></p>

<p>One of the less frequent use cases for the macro is creating two FAQ pages
with some overlapping content, and some exclusive content.  For example, image
you need to provide a FAQ for managers and another for employees.  There may be
some overlapping questions, such as &quot;How do I use Confluence&quot;, but
some questions may only be appropriate for one or the other group, such as
&quot;How do I give an employee a bonus?&quot;.  The <strong>faq</strong> macro
can accommodate this situation in a number of ways, one of which is by using
labels to pull in the pages.  For example, there could be one faq page which
pulls in all pages with the labels "faq" or "faq-manager" and another that
pulls in pages with the labels "faq" or "faq-employee".</p>

<p>While the initial use case was to create a FAQ, the same macro can be used to
present lots of other types of information.  The basic functionality of the plugin is to
collect pages via a number of selection patterns, and present some or all of the content
on those pages to the user in one of a few different styles.

<h3>Selecting Content</h3>

<p>The macro uses three fields to locate content:<strong>Labels</strong>,
<strong>Space Keys</strong>, <strong>Page</strong>.</p>

<h4>Labels</h4>

<p>The Labels field accepts one or more labels.  By default, if
one or more labels is provide, content with any of the labels will be selected;
however, if the <strong>And Labels</strong> box is checked, only content with
all of the labels will be selected</p>

<h4>Space Keys</h4>

<p>The Space Keys field accepts a comma separated list of space keys.  If set, only content
in the listed spaces will be selected.</p> 

<p>The space keys field has an autocomplete assistant. When typing in the name
of a space, a dropdown of matching space names will be shown.  If a space is
selected in the dropdown, the key for that space will be inserted into the
field.  At present, the autocomplete expects to identify a single space,
so to select two spaces you will need to select the first, copy the space key,
select the second and then insert a comma and the copied space key.</p>

<h4>Page Title</h4>

<p>If set, only content which is a descendant of the page will be found</p>

<h4>Summary of Content Selection</h4>

<table class="confluenceTable">
   <thead">
      <tr>
         <th class="confluenceTh">Label</th>
         <th class="confluenceTh">Space Keys</th>
         <th class="confluenceTh">Page</th>
         <th class="confluenceTh">Expected Outcome</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">The children of the current page will be selected. (All descendants will be selected if <strong>Include Descendants</strong> is checked.)</td>
      </tr>
      <tr>
         <td class="confluenceTd">$checkAsHtml</td>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">Every page, regardless of space, with any of the labels will be included.  If <strong>And Labels</strong> is checked, then only pages with all of the labels will be included.</td>
      </tr>
      <tr>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">$checkAsHtml</td>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">The home page and every descendant page for every space will be selected. <strong>Use this with caution as it can pull in a lot of content.</strong>.</td>
      </tr>
      <tr>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">?</td>
         <td class="confluenceTd">$checkAsHtml</td>
         <td class="confluenceTd">Child or Descendant pages of the selected page will be included.  The Space Keys field will be ignored in this case as the page defines the bounding space.</td>
      </tr>
      <tr>
         <td class="confluenceTd">$checkAsHtml</td>
         <td class="confluenceTd">$checkAsHtml</td>
         <td class="confluenceTd">-</td>
         <td class="confluenceTd">All pages with any/all of the labels that are also in the idenfied spaces will be included.</td>
      </tr>
      <tr>
         <td class="confluenceTd">$checkAsHtml</td>
         <td class="confluenceTd">?</td>
         <td class="confluenceTd">$checkAsHtml</td>
         <td class="confluenceTd">All pages with any/all of the labels that are also a descendant of the identified page will be included.</td>
      </tr>
   </tbody>
</table>

<p>Legend: $checkAsHtml = field has some content; &quot;-&quot; = field is empty; &quot;?&quot; = field is ignored.</p>

<h3>Render Style</h3>

<p>The <strong>faq</strong> macro can render in one of three ways: Expand,
Popup and Link.  The Expand Style will generate what appears to be a list of 
<strong>expand</strong> macros.  The Popup and Link styles will generate a 
table. When the title of the page is clicked with the Popup style, the content
will be rendered in a popup dialog.  When using the Link style, the browser will
navigate to the page.</p>

<p><img src="$expandImage" width="500px" alt="View of FAQ macro with Render Style set to Expand" /></p>
<p><img src="$linkImage" width="500px" alt="View of FAQ macro with Render Style set to Link or Popup." /></p>
<p><img src="$popupImage" width="500px" alt="View of FAQ macro with Dialog Popup." /></p>

<p>If the <strong>Render Content</strong> value is set to either <strong>First Excerpt</strong>
or <strong>First Shared Block</strong>, then a button that will navigate to the page is provided.
<p>If the <strong>Render Content</strong> value is set to <strong>Page</strong>, no link is 
provided as the user can already see all of the content.</p>

<h3>Render Content</h3>

<p>When getting the content from the include pages, the macro can pull in the <strong>First Excerpt</strong>, 
<strong>First Shared Block</strong> or the full <strong>Page</strong> contents.</p>

<h3>Extra Page Information</h3>

<p>Some extra information about the page can be provided as well.  In the Expand Render Style, the extra
information is shown after the user expands the link.  For the Link and Popup mode, the extra information
is shown as more columns in the initial table</p>

<p><img src="$expandAllItemsImage" width="500px" alt="View of FAQ macro with Render Style set to Expand" /></p>
<p><img src="$linkAllItemsImage" width="500px" alt="View of FAQ macro with Render Style set to Expand" /></p>

<h4>Parameters</h4>

<p><strong>Labels</strong>: Zero or more labels.</p>
<p><strong>Restrict to Spaces</strong>: Zero or more comma separated space keys.</p>
<p><strong>Restrict to Descendants of Page</strong>: Restricts the search to content that is a descendant of the identified page.</p>
<p><strong>Render Style</strong>: One of <strong>Expand</strong> (default), <strong>Popup</strong> or <strong>Link</strong>.</p>
<p><strong>Render Content</strong>: One of <strong>Page</strong> (default), <strong>First Excerpt</strong> or <strong>First Shared Block</strong>.</p>
<p><strong>Title on Popup Dialog</strong>: Text to display in the title section of the popup dialog.  Default to the page title.  The string $title will be replace by the page title.</p>
<p><strong>Add Labels</strong>: If set, the list of labels will be joined with &quot;and&quot; rather than &quot;or&quot;.</p>
<p><strong>Include Descendants</strong>: When pulling children pages, all descendants will be included.</p>
<p><strong>Include Space</strong>: Show the space in the results.</p>
<p><strong>Include Last Modified Date</strong>: Show the last modified date in the results.</p>
<p><strong>Include Last Modification Author</strong>: Show who made the last modification in the results.</p>
<p><strong>Include Page Labels</strong>: Show page labels in the results.</p>
<p><strong>Include Like Count</strong>: Show the number of like in the results.</p>

